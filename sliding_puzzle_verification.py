# https://www.codewars.com/kata/5e28b3ff0acfbb001f348ccc
# 2020/06/25 [yetanotherjohndoe]

def is_solved(board):
    _start = 0

    # check if board starts with 0, otherwise it's a waste of time anyway
    if board[0][0] == _start:

        # determin boardsize
        _board_rows = len(board)
        _board_columns = len(board[0])
        _end = _board_rows * _board_columns

        _current = 0

        for _row in board:
            for _cell in _row:
                if _cell == _current:
                    _current += 1
                else:
                    break

        if _end == _current:
            return True

    else:
        return False
