# https://www.codewars.com/kata/593c0ebf8b90525a62000221
# 2020/06/23 [yetanotherjohndoe]

def group_groceries(groceries):
    # convert string to list
    _list = groceries.split(',')

    # inventory
    _catalogue = {
        'fruit': [],
        'meat': [],
        'other': [],
        'vegetable': [],
    }

    # mix and match items to category
    for _entry in _list:
        _k, _v = _entry.split('_')
        if _k in _catalogue:
            _catalogue[_k].append(_v)
        else:
            _catalogue['other'].append(_v)

    # building the output string
    _string = ''

    for _group in _catalogue:
        _string += f'{_group}:'
        _string += f'{",".join(sorted(_catalogue[_group]))}\n'

    return _string[:-1]