# https://www.codewars.com/kata/59cfc09a86a6fdf6df0000f1
# 2020/06/28 [yetanotherjohndoe]

def capitalize(s, ind):
    _s = list(s)

    for _ind in ind:
        try:
            _s[_ind] = _s[_ind].upper()
        except:
            pass

    return ''.join(_s)
