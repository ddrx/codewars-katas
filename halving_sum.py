# https://www.codewars.com/kata/5a58d46cfd56cb4e8600009d
# 2020/06/23 [yetanotherjohndoe]

def halving_sum(n):
    _n = n
    _sum = 0

    _divisor = 2

    # check if _n (from _previous_ iteration) is at least 1
    while _n > 0:
        # add to sum
        _sum += _n
        # prepare _n for next iteration
        _n = int(n / _divisor)
        _divisor *= 2

    # return sum
    return _sum