# https://www.codewars.com/kata/52608f5345d4a19bed000b31
# 2020/06/28 [yetanotherjohndoe]

from decimal import Decimal


def to_chinese_numeral(n):

    # setup
    _chinese = ""

    _n = Decimal(n)
    _n_length = len(str(n))

    _negative = False
    _decimal = False
    _zerogrouping = False

    # if negative number
    if _n < 0:
        _n *= -1
        _negative = True

    # if decimal number
    if int(_n) != _n:

        _decimal = True

        _integral, _fraction = str(n).split(".")

        if _integral[0] == "-":
            _integral = _integral[1:]

        _n = Decimal(_integral)

        _chinese_f = ""

    _n_length = len(str(_n))

    # chinese numerals
    numerals = {
        "-": "负",
        ".": "点",
        0: "零",
        1: "一",
        2: "二",
        3: "三",
        4: "四",
        5: "五",
        6: "六",
        7: "七",
        8: "八",
        9: "九",
        10: "十",
        100: "百",
        1000: "千",
        10000: "万",
    }

    # convert numbers
    if _n_length == 1:
        _chinese = numerals[_n]

    elif _n_length > 1 and _n_length <= 5:
        _pos = _n_length
        _char = 0
        _number = str(_n)

        _group = [None, None, 10, 100, 1000, 10000]  # None for Padding

        # special case for 11-19
        if _n <= 19:
            _chinese = f"{numerals[10]}{numerals[int(str(_n)[-1])]}"

        # all others
        else:
            while _pos > 1:

                if int(_number[_char]) > 0:
                    _chinese += (
                        f"{numerals[int(_number[_char])]}{numerals[_group[_pos]]}"
                    )

                    _zerogrouping = False

                # special case for zero
                else:
                    if not _zerogrouping:
                        _chinese += f"{numerals[0]}"
                        _zerogrouping ^= True

                _pos -= 1
                _char += 1

            if int(_number[_char]) > 0:
                _chinese += f"{numerals[int(_number[-1])]}"

        # remove trailing zero
        if len(_chinese) > 1 and _chinese[-1] == str(numerals[0]):
            _chinese = _chinese[:-1]

    # process the fraction
    if _decimal:
        for _int in _fraction:
            _chinese_f += numerals[int(_int)]

        _chinese = f'{numerals["."]}'.join([_chinese, _chinese_f])

    # prepend minus if negative
    if _negative:
        _chinese = f'{numerals["-"]}{_chinese}'

    print(f"{n} -> {_chinese}")
    return _chinese
