# https://www.codewars.com/kata/59cf0ba5d751dffef300001f
# 2020/06/28 [yetanotherjohndoe]

def vertical_histogram_of(s):
    # only process upercase characters, also sorting it while at it
    _s = sorted([_char for _char in s if _char.isupper()])

    # only process non-empty
    if _s:
        # dictionary holding characters and number of appearance
        _count = dict()
        _seen = set()

        # iterate over characters and count
        for _char in _s:
            if _char not in _seen:
                _seen.add(_char)
                _count[_char] = 1
            else:
                _count[_char] += 1

        _histogram = []

        # looping over the results, creating histogram
        _max = max(_count.values())

        while _max > 0:
            _histogram_row = []
            for _char in _count:
                if _count[_char] >= _max:
                    _histogram_row.append('*')
                else:
                    _histogram_row.append(' ')

            _histogram.append(_histogram_row)

            _max -= 1

        # add legend
        _histogram.append([_char for _char in _count])

        # print histogram
        # for _row in _histogram:
        #    print(' '.join(_row))

        # join pieces, strip spaces on the right side
        _histogram = [' '.join(_row).rstrip() for _row in _histogram]

        return '\n'.join(_histogram)

    # otherwise return empty string
    else:
        return str()
