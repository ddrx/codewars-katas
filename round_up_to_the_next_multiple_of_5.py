# https://www.codewars.com/kata/55d1d6d5955ec6365400006d
# 2020/06/23 [yetanotherjohndoe]

def round_to_next5(n):
    # get modulus of input
    _mod = n % 5

    # if modulus not zero
    if _mod:
        # subtract modulus from input and add 5
        return n - _mod + 5

    # if modulus zero
    else:
        # return input
        return n