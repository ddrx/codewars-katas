# https://www.codewars.com/kata/5412509bd436bd33920011bc
# 2020/06/27 [yetanotherjohndoe]

def maskify(cc):
    if len(cc) > 4:

        _xx = len(cc[:-4]) * '#'
        _cc = cc[-4:]

        return ''.join([_xx, _cc])

    else:

        return cc
