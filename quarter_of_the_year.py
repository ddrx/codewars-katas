# https://www.codewars.com/kata/5ce9c1000bab0b001134f5af
# 2020/06/25 [yetanotherjohndoe]

def quarter_of(month):
    for _q in range(1, 5):
        if float(month / 12) <= float(0.25 * _q):
            return _q
