# https://www.codewars.com/kata/5bce125d3bb2adff0d000245
# 2020/06/24 [yetanotherjohndoe]

# Decimal() is the way to go, float() is dangerous!!
from decimal import Decimal


def london_city_hacker(journey):
    # bool to keep track if bus ride is adjecent
    _adjbusride = False

    # list of tickets
    _tickets = [Decimal('0.00')]

    for _vehicle in journey:

        # check if str or int
        if isinstance(_vehicle, str):
            _adjbusride = False

            _tickets.append(Decimal('2.40'))

        else:
            if _adjbusride:
                # if current is an adjecent busride, do nothing. just set the bool back to false.
                _adjbusride ^= True

            else:
                _tickets.append(Decimal('1.50'))
                _adjbusride ^= True

    # return sum of tickets with currency prefix
    return f'£{Decimal(sum(_tickets))}'
