# https://www.codewars.com/kata/54da5a58ea159efa38000836
# 2020/06/23 [yetanotherjohndoe]

def find_it(_seq):
    # instance lookup table
    _lookup = {}

    # count occurances
    for _int in _seq:
        if _int in _lookup.keys():
            _lookup[_int] += 1
        else:
            _lookup[_int] = 1

    # check for odd number of occurances
    for _index, _int in enumerate(_lookup):
        if _lookup[_int] % 2:
            return _int