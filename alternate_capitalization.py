# https://www.codewars.com/kata/59cfc000aeb2844d16000075
# 2020/06/23 [yetanotherjohndoe]

def capitalize(_str):
    # resulting string
    _txt = ''

    # for each character determin even/odd and add to result
    for _index, _char in enumerate(_str):
        if (_index % 2):
            _txt += _char.lower()
        else:
            _txt += _char.upper()

    # return result and result with swapped cases
    return [_txt, _txt.swapcase()]