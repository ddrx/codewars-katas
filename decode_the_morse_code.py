# https://www.codewars.com/kata/54b724efac3d5402db00065e
# 2020/06/23 [yetanotherjohndoe]

def decodeMorse(morse_code):
    # adding 'space' to the MORSE_CODE table
    MORSE_CODE['_'] = str(' ')

    # remove leading and trailing whitespace
    _morse = morse_code.lstrip()
    _morse = _morse.rstrip()
    # make spaces 'visible'
    _morse = _morse.replace('   ', ' _ ')
    # split string into morse character
    _morse = _morse.split(' ')

    # list of decoded characters
    _txt = []

    # decoding
    for _char in _morse:
        _txt.append(MORSE_CODE[_char])

    # return joined string
    return ''.join(_txt)