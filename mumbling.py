# https://www.codewars.com/kata/5667e8f4e3f572a8f2000039
# 2020/06/22 [yetanotherjohndoe] r1

def accum(s):
    # empty list to store mumble
    _mumble = []

    # iterate over characters, set case and number of repetition
    for _index, _char in enumerate(s):
        _mumble.append(f'{_char.upper()}{_char.lower() * _index}')

    # return all the pieces joined together
    return '-'.join(_mumble)