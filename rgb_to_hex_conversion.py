# https://www.codewars.com/kata/513e08acc600c94f01000001
# 2020/06/23 [yetanotherjohndoe]

def rgb(*_rgb):
    # final string
    _hex = str()

    # iterate over given values, checking range and converting to uppercase hex
    for _val in _rgb:
        if _val < 0:
            _val = 0
        elif _val > 255:
            _val = 255

        _hex += f'{_val:02x}'.upper()

    # return 6 character hex value
    return _hex