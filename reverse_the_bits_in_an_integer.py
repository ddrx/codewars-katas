# https://www.codewars.com/kata/5959ec605595565f5c00002b
# 2020/06/28 [yetanotherjohndoe]

def reverse_bits(n):
    return int( str( str( bin(n) )[2:] )[::-1] , 2)
