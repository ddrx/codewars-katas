# https://www.codewars.com/kata/52840d2b27e9c932ff0016ae
# 2020/06/23 [yetanotherjohndoe]

def locate(seq, value, found=0):
    if found:
        return True

    elif not found:

        for _sub in seq:
            if value == _sub:
                # print(f'found {_sub}')
                found = 1
                break
            elif isinstance(_sub, list):
                # print(f'sublist: {_sub}')

                # returning the functions result into found to break out of the higher loop
                found = locate(_sub, value, found)

        # check if we found it now
        if found:
            return True

        else:
            # print('not found')
            return False