# https://www.codewars.com/kata/5264d2b162488dc400000001
# 2020/06/24 [yetanotherjohndoe]

def spin_words(sentence):
    # split input into single words
    _words = sentence.split(' ')

    # check length of each word, if length >= 5 reverse
    for _index, _word in enumerate(_words):
        if len(_word) >= 5:
            _words[_index] = _word[::-1]

    # join words and return sentence
    return ' '.join(_words)