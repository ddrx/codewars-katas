[![DDRX](https://img.shields.io/badge/DD-RX-333?style=for-the-badge)](https://ddrx.ch)
***

## [codewars](https://www.codewars.com/r/GzDG2Q) katas

[![codewars](https://www.codewars.com/users/yetanotherjohndoe/badges/micro)](https://www.codewars.com/users/yetanotherjohndoe/completed)

https://www.codewars.com/users/yetanotherjohndoe/completed

---

[![Made with Python](https://img.shields.io/badge/made%20with-python_3.6-yellowgreen.svg?style=flat-square&logo=python&logoColor=white)](https://www.python.org/)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg?style=flat-square&logo=gnu&logoColor=white)](https://www.gnu.org/licenses/gpl-3.0.en.html)