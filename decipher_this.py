# https://www.codewars.com/kata/581e014b55f2c52bb00000f8
# 2020/06/28 [yetanotherjohndoe]

def decipher_this(msg):
    _words = msg.split(' ')

    _deciphered = []

    for _index, _word in enumerate(_words):

        try:
            _unicode = int(_word[:3])
        except:
            _unicode = int(_word[:2])

        _tmp = [chr(_unicode)]

        if _unicode > 99:
            _tmp.extend(list(_word[3:]))

        elif _unicode <= 99:
            _tmp.extend(list(_word[2:]))

        # swap second and last character (if string longer than one)
        if len(_tmp) > 1:
            _tmp[1], _tmp[-1] = _tmp[-1], _tmp[1]

        _deciphered.append(''.join(_tmp))

    return ' '.join(_deciphered)
