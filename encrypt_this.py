# https://www.codewars.com/kata/5848565e273af816fb000449
# 2020/06/28 [yetanotherjohndoe]

def encrypt_this(text):
    # placeholder in case input is empty
    _encrypted = ''

    if text:
        # split input into list
        _words = text.split()

        _encrypted = []

        # iterate over each word, swapping the first character for its unicode value and swapping second with last char
        for _word in _words:

            _tmp = []

            _tmp.append(str(ord(_word[0])))
            _tmp.extend(_word[1:])

            if len(_tmp) > 1:
                _tmp[1], _tmp[-1] = _tmp[-1], _tmp[1]

            _encrypted.append(''.join(_tmp))

    return ' '.join(_encrypted)
