# https://www.codewars.com/kata/5263c6999e0f40dee200059d
# 2020/06/26 [yetanotherjohndoe]

from itertools import product


def get_pins(observed):
    # set up keypad and determin adjecent numbers

    _keypad = [[0, 8]]  # adding 0 (and its only neighbour 8) manually

    _lowest = 1
    _highest = 9

    for _i in range(_lowest, _highest + 1):

        _temp = [_i]

        # horizontal
        if _i % 3 == 1:  # right, +1
            _temp.append(_i + 1)
        elif _i % 3 == 2:  # left and right, +1 -1
            _temp.extend([_i + 1, _i - 1])
        elif _i % 3 == 0:  # left, -1
            _temp.append(_i - 1)

        # vertical
        if _i + 3 <= _highest:  # below, +3
            _temp.append(_i + 3)

        if _i - 3 >= _lowest:  # above, -3
            _temp.append(_i - 3)

        if _i == 8:
            _temp.append(0)

        # sort and append
        _keypad.append(sorted(_temp))

    # resulting in:
    # _keypad = [[0, 8], [1, 2, 4], [1, 2, 3, 5], [2, 3, 6], [1, 4, 5, 7], [2, 4, 5, 6, 8], [3, 5, 6, 9], [4, 7, 8], [0, 5, 7, 8, 9], [6, 8, 9]]

    # convert integer to string
    _keypad = [[str(_n) for _n in _s] for _s in _keypad]

    # possible combinations based on observation
    _observed = [_keypad[int(_c)] for _c in str(observed)]

    # generate combinations
    _combinations = [''.join(s) for s in product(*_observed)]

    # debug
    # print(f'observed: {observed}, possibilities: {_observed}, combinations: {_combinations}')

    return _combinations
