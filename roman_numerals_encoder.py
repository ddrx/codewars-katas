# https://www.codewars.com/kata/51b62bf6a9c58071c600001b
# 2020/06/28 [yetanotherjohndoe]

def solution(n):
    # convert input to string
    _n = str(n)

    # list for resulting roman numbers
    _r = []

    # I 1  V 5  X 10  L 50  C 100  D 500  M 1000
    _roman = [['I', 'V'], ['X', 'L'], ['C', 'D'], ['M', '-']]

    for _index, _number in enumerate(_n[::-1]):

        # skipping zero values
        if int(_number):

            # building roman numbers and append to list
            _int = int(_number)
            if _int < 4:
                _r.append(_roman[_index][0] * _int)
            elif _int >= 4 and _int < 9:
                _r.append(f'{_roman[_index][0] * (5 - _int)}{_roman[_index][1]}{_roman[_index][0] * (_int - 5)}')
            elif _int == 9:
                _r.append(f'{_roman[_index][0]}{_roman[_index + 1][0]}')

    # return joirned string
    return ''.join(_r[::-1])
