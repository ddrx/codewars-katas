# https://www.codewars.com/kata/5390bac347d09b7da40006f6
# 2020/06/25 [yetanotherjohndoe]
import string as stringmod

def to_jaden_case(string):

    return stringmod.capwords(string)